#if !defined TCC_CONFIG_H
#define TCC_CONFIG_H

#include "TCC-Config.h" // cmake generated

#define POL_X			0
#define POL_Y			1
#define NR_POLARIZATIONS	2

#if !defined NR_TIMES
#define NR_TIMES		768
#endif

#define REAL			0
#define IMAG			1
#define COMPLEX			2

#endif
