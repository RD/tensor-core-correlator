#include "test/Common/Record.h"


#if defined MEASURE_POWER
Record::Record(pmt::PMT &powerSensor)
:
  powerSensor(powerSensor)
{
}
#endif


#if defined MEASURE_POWER
void Record::getPower(CUstream, CUresult, void *userData)
{
  Record *record = (Record *) userData;
  record->state = record->powerSensor.Read();
}
#endif


void Record::enqueue(cu::Stream &stream)
{
  stream.record(event); // if this is omitted, the callback takes ~100 ms ?!

#if defined MEASURE_POWER
#if 0 && defined PMT_BUILD_TEGRA // no NVIDIA library calls allowed in callback
  stream.synchronize();
  state = powerSensor.Read();
#else
  stream.addCallback(&Record::getPower, this);
#endif
#endif
}
