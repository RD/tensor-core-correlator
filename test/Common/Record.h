#if !defined RECORD_H
#define RECORD_H

#include "test/Common/Config.h"

#include <cudawrappers/cu.hpp>

#if defined MEASURE_POWER
#include <pmt.h>
#endif


struct Record
{
  public:
#if defined MEASURE_POWER
    Record(pmt::PMT &);
#endif

    void enqueue(cu::Stream &);

    mutable cu::Event event;

#if defined MEASURE_POWER
    pmt::PMT   &powerSensor;
    pmt::State state;

  private:
    static void getPower(CUstream, CUresult, void *userData);
#endif
};

#endif
