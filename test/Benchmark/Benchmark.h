#if !defined CORRELATOR_TEST_H
#define CORRELATOR_TEST_H

#include "test/Common/ComplexInt4.h"
#include "test/Common/UnitTest.h"
#include "util/multi_array.h"

#include <cuda_fp16.h>


class Benchmark : public UnitTest
{
  public:
    Benchmark();

  private:
    template<typename SampleType, typename VisibilityType> void doTest(unsigned nrBits, unsigned nrReceiversPerBlock, unsigned nrReceivers);
    template<typename SampleType>                          void setTestPattern(const multi_array::array_ref<SampleType, 5> &samples);

    template<typename SampleType> static SampleType randomValue();
    template<typename VisibilityType> bool approximates(const VisibilityType &a, const VisibilityType &b) const;

    unsigned	    capability;
    bool	    hasIntegratedMemory;
};


template<> complex_int4_t Benchmark::randomValue<complex_int4_t>()
{
  return complex_int4_t((int) (15 * drand48()) - 7, (int) (15 * drand48()) - 7);
}


template<> std::complex<int8_t> Benchmark::randomValue<std::complex<int8_t>>()
{
  return std::complex<int8_t>((int) (255 * drand48()) - 127, (int) (255 * drand48()) - 127);
}


template<> std::complex<__half> Benchmark::randomValue<std::complex<__half>>()
{
  return std::complex<__half>(drand48() - .5, drand48() - .5);
}

template <typename VisibilityType> bool Benchmark::approximates(const VisibilityType &a, const VisibilityType &b) const
{
  return a == b;
}

#endif
