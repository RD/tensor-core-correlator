#include "libtcc/Correlator.h"
#include "test/Benchmark/Benchmark.h"
#include "test/Common/ComplexInt4.h"
#include "test/Common/Record.h"
#include "util/ExceptionPropagator.h"

#include <cstdlib>
#include <cstring>
#include <iostream>

#include <cudawrappers/nvrtc.hpp>

#define GNU_SOURCE
#include <link.h>
#include <omp.h>

Benchmark::Benchmark()
:
  UnitTest(0), // device number
  capability(10 * device.getAttribute<CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR>() + device.getAttribute<CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR>()),
  hasIntegratedMemory(device.getAttribute(CU_DEVICE_ATTRIBUTE_INTEGRATED))
{
  ExceptionPropagator ep;

#pragma omp parallel num_threads(1) // using more threads yields unreliable performance
  ep([&] () {
    context.setCurrent();

    for (unsigned nrBits: {8, 16, 4})
#pragma omp for collapse(2) schedule(dynamic) ordered
      for (unsigned nrReceivers = 1; nrReceivers <= 576; nrReceivers ++)
	for (unsigned nrReceiversPerBlock = 32; nrReceiversPerBlock <= 64; nrReceiversPerBlock += 16)
	  if (nrBits ==  4 && (capability >= 73 && capability <= 89) ||
	      nrBits ==  8 && capability >= 72 ||
	      nrBits == 16 && capability >= 70)
	    switch (nrBits) {
	      case  4 : doTest<complex_int4_t, std::complex<int32_t>>(4, nrReceiversPerBlock, nrReceivers);
			break;

	      case  8 : doTest<std::complex<int8_t>, std::complex<int32_t>>(8, nrReceiversPerBlock, nrReceivers);
			break;

	      case 16 : doTest<std::complex<__half>, std::complex<float>>(16, nrReceiversPerBlock, nrReceivers);
			break;
	    }

    stream.synchronize();
  });
}


template <typename SampleType, typename VisibilityType> void Benchmark::doTest(unsigned nrBits, unsigned nrReceiversPerBlock, unsigned nrReceivers)
{
  constexpr double   measureTime         = 3; // seconds
	    unsigned nrChannels          = 4 * device.getAttribute<CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT>(); // provide enough parallelism
  constexpr unsigned nrPolarizations     = 2;
  constexpr unsigned nrSamplesPerChannel = 3072;
  constexpr bool     addVisibilities     = false;
            unsigned nrTimesPerBlock     = 128 / nrBits;
	    unsigned nrBaselines         = nrReceivers * (nrReceivers + 1) / 2;

  tcc::Correlator correlator(device, nrBits, nrReceivers, nrChannels, nrSamplesPerChannel, nrPolarizations, nrReceiversPerBlock);
  unsigned repeatCount;

  multi_array::extent<5> samplesExtent(multi_array::extents[nrChannels][nrSamplesPerChannel / nrTimesPerBlock][nrReceivers][nrPolarizations][nrTimesPerBlock]);
  multi_array::extent<4> visibilitiesExtent(multi_array::extents[nrChannels][nrBaselines][nrPolarizations][nrPolarizations]);

  cu::HostMemory hostSamples(sizeof(SampleType) * samplesExtent.size /*, CU_MEMHOSTALLOC_WRITECOMBINED */);
  cu::HostMemory hostVisibilities(sizeof(VisibilityType) * visibilitiesExtent.size);

  multi_array::array_ref<SampleType, 5> samplesRef(* (SampleType *) hostSamples, samplesExtent);
  multi_array::array_ref<VisibilityType, 4> visibilitiesRef(* (VisibilityType *) hostVisibilities, visibilitiesExtent);

  setTestPattern<SampleType>(samplesRef);

 // do not use powerSensor from UnitTest (causes problems on Grace Hopper after a long time of measuring
#if defined MEASURE_POWER
#if 0 && defined PMT_BUILD_NVML
  std::unique_ptr<pmt::PMT> powerSensor(pmt::nvml::NVML::Create()); // do not use UnitTest::powerSensor for a long time on Grace Hopper
#endif
  Record computeRecordStart(*powerSensor), computeRecordStop(*powerSensor);
#else
  Record computeRecordStart, computeRecordStop;
#endif

  ExceptionPropagator ep;

#pragma omp critical (GPU) // TODO: use multiple locks when using multiple GPUs
  ep([&] () {
    cu::DeviceMemory deviceSamples(hasIntegratedMemory ? cu::DeviceMemory(hostSamples) : cu::DeviceMemory(sizeof(SampleType) * samplesExtent.size));
    cu::DeviceMemory deviceVisibilities(hasIntegratedMemory ? cu::DeviceMemory(hostVisibilities) : cu::DeviceMemory(sizeof(VisibilityType) * visibilitiesExtent.size));

    if (!hasIntegratedMemory)
      stream.memcpyHtoDAsync(deviceSamples, hostSamples, samplesRef.bytesize());

    double endTime = omp_get_wtime() + measureTime;
    computeRecordStart.enqueue(stream);

    for (repeatCount = 0; omp_get_wtime() < endTime; repeatCount ++) {
      correlator.launchAsync(stream, deviceVisibilities, deviceSamples, addVisibilities);
      stream.synchronize();
    }

    computeRecordStop.enqueue(stream);
  });

  stream.synchronize();

  char msg[64];
  sprintf(msg, "bits=%u recv/blk=%u recv=%u cnt=%u", nrBits, nrReceiversPerBlock, nrReceivers, repeatCount);
#pragma omp ordered
  report(msg, computeRecordStart, computeRecordStop, repeatCount * correlator.FLOPS());
}


template<typename SampleType> void Benchmark::setTestPattern(const multi_array::array_ref<SampleType, 5> &samples)
{
  SampleType randomValues[7777]; // use a limited set of random numbers to save time

  for (unsigned i = 0; i < 7777; i ++)
    randomValues[i] = randomValue<SampleType>();

  unsigned i = 0;

  for (SampleType &sample : samples)
    sample = randomValues[i ++ % 7777U];
}


int main(int argc, char *argv[])
{
  int err = 0;

  try {
    cu::init();
    Benchmark benchmark;
  } catch (cu::Error &error) {
    std::cerr << "cu::Error: " << error.what() << std::endl;
    err = 1;
  } catch (nvrtc::Error &error) {
    std::cerr << "nvrtc::Error: " << error.what() << std::endl;
    err = 1;
  }

  return err;
}
