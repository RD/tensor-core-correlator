project(OpenCLCorrelatorTest)
find_package(OpenMP REQUIRED)
find_package(OpenCL)

if(${OpenCL_FOUND})
  set(KERNEL_SOURCE_FILE "${CMAKE_SOURCE_DIR}/libtcc/kernel/TCCorrelator.cu")
  add_executable(${PROJECT_NAME})
  target_sources(${PROJECT_NAME} PRIVATE OpenCLCorrelatorTest.cc)
  target_include_directories(
    ${PROJECT_NAME} PRIVATE ${CMAKE_SOURCE_DIR} ${OpenCL_INCLUDE_DIRS}
  )
  target_link_libraries(
    ${PROJECT_NAME} PRIVATE ${OpenCL_LIBRARIES} OpenMP::OpenMP_CXX
  )
  target_compile_definitions(
    ${PROJECT_NAME} PRIVATE KERNEL_SOURCE_FILE="${KERNEL_SOURCE_FILE}"
  )

endif()
