project(example)
cmake_minimum_required(VERSION 3.17 FATAL_ERROR)

find_package(CUDAToolkit REQUIRED)
find_package(cudawrappers REQUIRED)
find_package(libtcc REQUIRED)

add_executable(${PROJECT_NAME} example.cpp)
target_link_libraries(${PROJECT_NAME} tcc cudawrappers::cu cudawrappers::nvrtc)
