#include <cudawrappers/cu.hpp>
#include <libtcc/Correlator.h>

#define NR_BITS 8
#define NR_CHANNELS 480
#define NR_POLARIZATIONS 2
#define NR_SAMPLES_PER_CHANNEL 3072
#define NR_RECEIVERS 576
#define NR_RECEIVERS_PER_BLOCK 64

int main(int argc, char *argv[]) {
  cu::init();
  cu::Device device(0);
  cu::Context context(0, device);
  context.setCurrent();
  tcc::Correlator correlator(NR_BITS, NR_RECEIVERS, NR_CHANNELS,
                             NR_SAMPLES_PER_CHANNEL, NR_POLARIZATIONS,
                             NR_RECEIVERS_PER_BLOCK);
}
