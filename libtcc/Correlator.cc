#include "libtcc/Correlator.h"

#include <filesystem>
#include <iostream>

#define GNU_SOURCE
#include <link.h>


extern const char _binary_libtcc_kernel_TCCorrelator_cu_start, _binary_libtcc_kernel_TCCorrelator_cu_end;

namespace tcc {

std::string Correlator::findNVRTCincludePath() const
{
  std::string path;

  if (dl_iterate_phdr([] (struct dl_phdr_info *info, size_t, void *arg) -> int
		      {
			std::string &path = *static_cast<std::string *>(arg);
			path = info->dlpi_name;
			return path.find("libnvrtc.so") != std::string::npos;
		      }, &path))
    for (size_t pos; (pos = path.find_last_of("/")) != std::string::npos;) {
      path.erase(pos); // remove last part of path
      
      if (std::filesystem::exists(path + "/include/cuda.h"))
	return path + "/include";
    }

  return ".";
}


Correlator::Correlator(const cu::Device &device,
		       unsigned nrBits,
		       unsigned nrReceivers,
		       unsigned nrChannels,
		       unsigned nrSamplesPerChannel,
		       unsigned nrPolarizations,
		       unsigned nrReceiversPerBlock,
		       const std::string &customStoreVisibility
		      )
:
  capability([&] { 
    return 10 * device.getAttribute<CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR>() + device.getAttribute<CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR>();
  } ()),
  nrReceiversPerBlock(nrReceiversPerBlock != 0 ? nrReceiversPerBlock : defaultNrReceiversPerBlock(nrReceivers)),
  correlatorModule(compileModule(nrBits, nrReceivers, nrChannels, nrSamplesPerChannel, nrPolarizations, this->nrReceiversPerBlock, customStoreVisibility)),
  correlatorKernel(correlatorModule, nrBits, nrReceivers, nrChannels, nrSamplesPerChannel, nrPolarizations, this->nrReceiversPerBlock)
{
}


unsigned Correlator::defaultNrReceiversPerBlock(unsigned nrReceivers) const
{
  return nrReceivers <=  32 ? 32 :
	 nrReceivers <=  48 ? 48 :
	 nrReceivers <=  64 || capability == 750 || capability == 870 ? 64 :
	 nrReceivers <=  96 ? 32 :
	 nrReceivers <= 128 ? 64 :
	 nrReceivers <= 144 && capability == 900 ? 48 :
	 nrReceivers <= 160 && capability == 900 ? 32 : 64;
}


cu::Module Correlator::compileModule(unsigned nrBits,
				     unsigned nrReceivers,
				     unsigned nrChannels,
				     unsigned nrSamplesPerChannel,
				     unsigned nrPolarizations,
				     unsigned nrReceiversPerBlock,
				     const std::string &customStoreVisibility
				    )
{
  std::vector<std::string> options =
  {
    "-I" + findNVRTCincludePath(),
    "-std=c++11",
    "-arch=compute_" + std::to_string(capability),
    "-lineinfo",
    "-DNR_BITS=" + std::to_string(nrBits),
    "-DNR_RECEIVERS=" + std::to_string(nrReceivers),
    "-DNR_CHANNELS=" + std::to_string(nrChannels),
    "-DNR_SAMPLES_PER_CHANNEL=" + std::to_string(nrSamplesPerChannel),
    "-DNR_POLARIZATIONS=" + std::to_string(nrPolarizations),
    "-DNR_RECEIVERS_PER_BLOCK=" + std::to_string(nrReceiversPerBlock),
  };

  if (!customStoreVisibility.empty())
    options.push_back("-DCUSTOM_STORE_VISIBILITY=" + customStoreVisibility);

  //std::for_each(options.begin(), options.end(), [] (const std::string &e) { std::cout << e << ' '; }); std::cout << std::endl;

  const std::string source(&_binary_libtcc_kernel_TCCorrelator_cu_start, &_binary_libtcc_kernel_TCCorrelator_cu_end);
  nvrtc::Program program(source, "TCCorrelator.cu");

  try {
    program.compile(options);
  } catch (nvrtc::Error &error) {
    std::cerr << program.getLog();
    throw;
  }

  //std::ofstream cubin("out.ptx");
  //cubin << program.getPTX().data();
  return cu::Module((void *) program.getPTX().data());
}


void Correlator::launchAsync(cu::Stream &stream, cu::DeviceMemory &visibilities, cu::DeviceMemory &samples, bool add)
{
  correlatorKernel.launchAsync(stream, visibilities, samples, add);
}


void Correlator::launchAsync(CUstream stream, CUdeviceptr visibilities, CUdeviceptr samples, bool add)
{
  cu::Stream _stream(stream);
  cu::DeviceMemory _visibilities(visibilities);
  cu::DeviceMemory _samples(samples);
  correlatorKernel.launchAsync(_stream, _visibilities, _samples, add);
}


uint64_t Correlator::FLOPS() const
{
  return correlatorKernel.FLOPS();
}

}
