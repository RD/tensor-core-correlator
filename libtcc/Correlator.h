#if !defined TCC_CORRELATOR_H
#define TCC_CORRELATOR_H

#include <string>

#include <cudawrappers/cu.hpp>
#include <cudawrappers/nvrtc.hpp>

#include "libtcc/CorrelatorKernel.h"

namespace tcc {
  class Correlator {
    public:
      Correlator(const cu::Device &,
		 unsigned nrBits,
		 unsigned nrReceivers,
		 unsigned nrChannels,
		 unsigned nrSamplesPerChannel,
		 unsigned nrPolarizations = 2,
		 unsigned nrReceiversPerBlock = 0, // 0: use a heuristic value that should work well
		 const std::string &customStoreVisibility = ""
		); // throw (cu::Error, nvrtc::Error)

      void launchAsync(cu::Stream &, cu::DeviceMemory &visibilities, cu::DeviceMemory &samples, bool add = false); // throw (cu::Error)
      void launchAsync(CUstream, CUdeviceptr visibilities, CUdeviceptr samples, bool add = false); // throw (cu::Error)

      uint64_t FLOPS() const;

    private:
      std::string      findNVRTCincludePath() const;
      unsigned         defaultNrReceiversPerBlock(unsigned nrReceivers) const;
      cu::Module       compileModule(unsigned nrBits,
				     unsigned nrReceivers,
				     unsigned nrChannels,
				     unsigned nrSamplesPerChannel,
				     unsigned nrPolarizations,
				     unsigned nrReceiversPerBlock,
				     const std::string &customStoreVisibility
				    );

      unsigned         capability;
      unsigned         nrReceiversPerBlock;
      cu::Module       correlatorModule;
      CorrelatorKernel correlatorKernel;
  };
}

#endif
